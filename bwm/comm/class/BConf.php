<?php

	class BConf
	{
		static $site_name = 'bwm';
		static $site_delimeter = ' : ';
		/*db*/
		static $db_host = 'host';
		static $db_name = 'name';
		static $db_user = 'user';
		static $db_pwd = 'pwd';

		/*upload*/
		static $upload = 'upload';
	}